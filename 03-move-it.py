#!/usr/bin/env python3

# For at man kan bruge python pakken til ev3
# robotten skal man importere den.
from ev3dev.ev3 import *
from time import sleep
import sys

ir = InfraredSensor('in2')
ir.mode = 'IR-PROX'

while True:
    if ir.value() > 60:
        print(ir.value())
        continue

    # Truck motoren
    liftMotor = MediumMotor('outD', stop_action='hold')

    # Hjul motore
    leftMotor = LargeMotor('outB', stop_action='brake')
    rightMotor = LargeMotor('outC', stop_action='brake')

    # Kør lidt frem.
    leftMotor.run_timed(time_sp=1500, speed_sp=500)
    rightMotor.run_timed(time_sp=1500, speed_sp=500)
    leftMotor.wait_while('running')

    # Løft pakken.
    liftMotor.run_timed(time_sp=2500, speed_sp=500)
    liftMotor.wait_while('running')

    # Drej 180 grader rundt.
    leftMotor.run_timed(time_sp=1000, speed_sp=-475)
    rightMotor.run_timed(time_sp=1000, speed_sp=475)
    leftMotor.wait_while('running')

    # Kør tilbage til start.
    leftMotor.run_timed(time_sp=1500, speed_sp=500)
    rightMotor.run_timed(time_sp=1500, speed_sp=500)
    leftMotor.wait_while('running')

    # Sæt pakken.
    liftMotor.run_timed(time_sp=2500, speed_sp=-500)
    liftMotor.wait_while('running')

    # Bak ud.
    leftMotor.run_timed(time_sp=1500, speed_sp=-500)
    rightMotor.run_timed(time_sp=1500, speed_sp=-500)
    leftMotor.wait_while('running')

    # Stop
    break

Sound.tone([(392, 350, 100)])
sys.exit()
