#!/usr/bin/env python3

from time import sleep
from ev3dev2.motor import OUTPUT_A, OUTPUT_B, OUTPUT_C
from ev3dev2.motor import MediumMotor, LargeMotor
from ev3dev2.motor import SpeedPercent, SpeedRPM, MoveSteering

from ev3dev2.sensor.lego import TouchSensor, InfraredSensor
from multiprocessing import Process

print("Ready!")

ts = TouchSensor()
ir = InfraredSensor()
sonar = MediumMotor(OUTPUT_A)
motors = [LargeMotor(address) for address in (OUTPUT_B, OUTPUT_C)]

def scan_rotator():
    while not ts.value():
        sonar.on_to_position(SpeedRPM(20), position=45)
        sonar.on_to_position(SpeedRPM(20), position=-45)
    stop()

def scan_value():
    while not ts.value():
        distance = ir.proximity
        if (distance < 20):
            direction = 'left'
            if (sonar.position < 0):
                direction = 'right'

            print('needs to turn .: ' + direction)
            turn(direction)
            sleep(1)

def run_direct():
    print('run direct...!')
    for m in motors:
        m.run_direct(duty_cycle_sp=70)

def turn(direction):
    for m in motors:
        m.stop(stop_action='brake')

    if ('right' == direction):
        m = LargeMotor(OUTPUT_C)
    else:
        m = LargeMotor(OUTPUT_B)

    m.run_direct(duty_cycle_sp=70)

    while 30 > ir.proximity:
        pass

    run_direct()

def stop():
    sonar.on_to_position(SpeedRPM(20), position=0)
    for m in motors:
        m.stop()

run_direct()

p1 = Process(target=scan_rotator)
p1.start()

p2 = Process(target=scan_value)
p2.start()
