#!/usr/bin/env python3

# For at man kan bruge python pakken til ev3
# robotten skal man importere den.
from ev3dev.ev3 import *

# Infrarød sensor koblet til.
ir = InfraredSensor()
# Sæt sensoren i Proximity mode - afstandsbedømmelse.
ir.mode = 'IR-PROX'

# Touch (knap) sensor koblet til.
ts = TouchSensor()

# Stor motor koblet til port B
leftMotor = LargeMotor('outB')

# Stor motor koblet til port C
rightMotor = LargeMotor('outC')

# Klasser er en måde at samle funktionalitet på, så
# ens programmer ikke (nødvendigvis) bliver en stor bunke rodekode.
class Robot:
    # 'def' er en deffinition af metoder/funktioner
    # Denne her (__init__) er en speciel metode også kaldet en "constructor".
    #
    # Koden i den vil blive eksekveret så snart en Klasse "initieres" (se linie 62)
    # Parameteren "self" er også en speciel parameter.
    # I python skal alle metoder der skal kunne "snakke med sig selv" have denne parameter som den første.
    def __init__(self, ir, ts, leftMotor, rightMotor):
        # Her sætter vi parameterene op, så de er tilgængelige for klassen.
        self.leftMotor = leftMotor
        self.rightMotor = rightMotor
        self.ir = ir
        self.ts = ts

    # Kør kommando :)
    def run(self):
        # Og vi startet ud med at få robotten til at kører fremad.
        self.runStraight()

        # Bliv ved med at køre indtil der trykkes på knappen.
        while not self.ts.value():
            # Hvis vi er tættere på end 50, så drej.
            if self.ir.value() < 50:
                self.turn()

    # Den her metode sætter begge motore op til at kører fuld kraft frem, indtil de får andet at vide.
    def runStraight(self):
        self.leftMotor.run_forever(speed_sp = self.leftMotor.max_speed)
        self.rightMotor.run_forever(speed_sp = self.rightMotor.max_speed)

    # Her drejer vi lidt ved at kører frem på det ene og baglæns på det andet hjul.
    def turn(self):
        self.leftMotor.run_timed(time_sp=80, speed_sp=600)
        self.rightMotor.run_timed(time_sp=80, speed_sp=-600)
        # Og vi siger til robotten at den skal fortsætte fremad.
        self.runStraight()


# Her laver vi en robot "robert" og sætter den igang.
robert = Robot(ir, ts, leftMotor, rightMotor)
robert.run();

# Når der stoppes, så gi' et bip.
Sound.beep()

# Og stop begge motore.
leftMotor.stop()
rightMotor.stop()
