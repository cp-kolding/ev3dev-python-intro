Guiden her tager udgangspunkt i at du:

1. Har en Lego Mindstorms™ robot.
1. Har flashet et SD-kort med [ev3dev](https://www.ev3dev.org/downloads/).
1. Har bygget [driving base](https://education.lego.com/en-us/support/mindstorms-ev3/building-instructions#robot) modellen, eller noget tilsvarende.

Guides til at forbinde til enheden kan du finder [her](https://www.ev3dev.org/docs/networking/). Dog kan jeg kun anbefale at købe en WIFI dongle og bruge den. Det er absolut det nemmeste og mest stabile.
