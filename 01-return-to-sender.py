#!/usr/bin/env python3

# For at man kan bruge python pakken til ev3
# robotten skal man importere den.
from ev3dev.ev3 import *

# Man skal importere alt man skal bruge i python,
# altså hvis det er udover de få globale metoder.
from time import sleep

# Stor motor koblet til port B
m1 = LargeMotor('outB')

# Stor motor koblet til port C
m2 = LargeMotor('outC')

# En af de 6 knapper.
btn = Button()

# Ved tryp på en af de 6 knapper stopper programmet.
counter = 0
while not btn.any():
    # Start begge motore, og lad dem kører i
    # 3000 millisekunder (ms) = 3 sekunter
    m1.run_timed(time_sp=3000, speed_sp=500)
    m2.run_timed(time_sp=3000, speed_sp=500)
    m2.wait_while('running')

    # Lad hver motor kører hver sin vej, i
    # 1100 ms - skulle gerne resultere i at robotten drejer (ca) 90°.
    m1.run_timed(time_sp=1100, speed_sp=500)
    m2.run_timed(time_sp=1100, speed_sp=-500)

    counter += 1
    if (counter == 4):
        # Vend rundt en sidste gang.
        m1.run_timed(time_sp=1100, speed_sp=500)
        m2.run_timed(time_sp=1100, speed_sp=-500)
        m2.wait_while('running')
        break

    # Hold en 2 sekunders pause før loopet kører forfra.
    # Bare fordi man kan....
    sleep(2)

# Stans robottens motore.
m1.stop(stop_action='hold')
m2.stop(stop_action='hold')
